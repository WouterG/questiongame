const AnswerPickState = {
    SHOW_ANSWERS: 1,
    VOTE_ANSWERS: 2,
    SHOW_SCORES: 3
};

module.exports = AnswerPickState;