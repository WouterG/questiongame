var ExplanationGameTotal = function(game, sh) {
    this.game = game;
    this.sh = sh;

    this.counter = 0;
    this.index = 0;
    this.lines = global.config.game_explanation;

    var that = this;

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() {
        return {
            line: that.lines[that.index]
        };
    };

    this.tick = function(game) {
        if (that.counter % 5 == 0) {
            that.index = that.counter / 5;
            if (that.index == that.lines.length) {
                that.game.gamestate.advanceState();
            } else {
                that.sendDataMaster();
            }
        }
    };
    this.start = function(game) {

    };

    this.allowPause = function() { return true; };
    this.onPause = function(pause) {};

    this.sendDataMaster = function() {
        that.sh.__sendDataMaster(that.getStateData());
    };
    this.sendDataPlayer = function(player) {};
    this.sendDataPlayers = function() {};
    this.sendDataAudienceMember = function(audience) {};
    this.sendDataAudience = function() {};
};

module.exports = ExplanationGameTotal;