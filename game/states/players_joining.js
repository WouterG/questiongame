var PlayersJoining = function(game, sh) {

    this.game = game;
    this.sh = sh;

    var that = this;

    this.getProtocolState = function() { return "init"; };
    this.getStateData = function() {
        return {

        };
    };

    this.tick = function(game) {

    };
    this.start = function(game) {
        that.sendDataMaster();
    };

    this.allowPause = function() { return false; };
    this.onPause = function(pause) {};

    this.sendDataMaster = function() {
        that.game.updatePlayerList();
    };
    this.sendDataPlayer = function(player) {};
    this.sendDataPlayers = function() {};
    this.sendDataAudienceMember = function(audience) {};
    this.sendDataAudience = function() {};

};

module.exports = PlayersJoining;