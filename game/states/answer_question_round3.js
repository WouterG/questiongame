
var AnswerQuestionRound3 = function(game, sh) {
    this.game = game;
    this.sh = sh;

    this.counter = 60;

    var that = this;

    var advanced = false;
    var advance = function() {
        if (advanced) {
            return;
        }
        advanced = true;
        that.game.gamestate.advanceState();
    };

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() {
        var players = [];
        for (var i = 0; i < that.game.players.length; i++) {
            var p = that.game.players[i];
            players.push({
                name: p.name,
                id: p.id,
                done: p.is_done_answering
            });
        }
        return {
            counter: that.counter,
            players: players
        };
    };

    this.tick = function(game) {
        that.counter--;
        if (that.counter == 0) {
            advance();
        } else {
            that.sendDataMaster();
        }
    };
    this.start = function(game) {
        for (var player in game.players) {
            player.is_done_answering = false;
            player.current_questions.clear();
        }

        game.question_history.concat(game.questions);
        game.questions = [];
        var newQuestion = null;

        while (newQuestion == null) {
            var q = global.get_random_question();
            if (!game.historyHasQuestion(q)) {
                newQuestion = q;
            }
        }

        var question = FinalQuestion(newQuestion);
        for (var player in game.players) {
            player.current_questions.push(question);
        }

        that.sendDataPlayers();
        that.sendDataAudience();
        that.sh.__sendDataMaster({
            question: that.game.current_questions[0].question
        });
    };

    this.onSocketMessage = function(socket, event, data) {
        if (socket.player == null || socket.game == null) {
            return;
        }
        if (socket.game != that.game) {
            return;
        }
        if (event == 'answer_change') {
            if (!global.has(data, "question_answer")) {
                console.error('Invalid answer_change request');
                return;
            }
            if (socket.player.is_done_answering) {
                return;
            }
            if (socket.player.current_questions.length < 1) {
                return;
            }
            var question = socket.player.current_questions[0];
            question.setAnswer(socket.player, data.question_answer);
        }
        if (event == 'answering_done') {
            if (socket.player.is_done_answering) {
                return;
            }
            if (socket.player.current_questions.length < 1) {
                return;
            }
            if (global.has(data, "question_answers")) {
                var question = socket.player.current_questions[0];
                question.setAnswer(socket.player, data.question_answers[0]);
            }
            socket.player.is_done_answering = true;
            var isEveryoneDone = true;
            for (var i = 0; i < that.game.players.length; i++) {
                if (!that.game.players[i].is_done_answering) {
                    isEveryoneDone = false;
                    break;
                }
            }
            if (isEveryoneDone) {
                that.counter = -1; // make sure we're past the timeout
                advance();
            }
        }
    };

    this.allowPause = function() { return true; };
    this.onPause = function(pause) {};

    this.sendDataMaster = function() {
        that.sh.__sendDataMaster(that.getStateData());
    };
    this.sendDataPlayer = function(player) {
        that.sh.__sendDataPlayer(player, {
            question: that.game.current_questions[0].question
        });
    };
    this.sendDataPlayers = function() {
        that.sh.__sendDataPlayers({
            question: that.game.current_questions[0].question
        });
    };
    this.sendDataAudienceMember = function(audience) {
        that.sh.__sendDataAudienceMember(audience);
    };
    this.sendDataAudience = function() {
        that.sh.__sendDataAudience();
    };
};

module.exports = AnswerQuestionRound3;