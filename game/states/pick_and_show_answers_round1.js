var PickAndShowAnswersRound1 = function(game, sh) {
    this.game = game;
    this.sh = sh;

    var that = this;

    this.question_index = 0;
    this.answer_pick_state = null;

    var SCORE_VOTE = 500;
    var SCORE_WIN_VOTE = 250;
    var SCORE_ALL_VOTES = 1000;

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() {
        return {

        };
    };

    this.tick = function(game) {

    };
    this.start = function(game) {
        game.questions = global.shuffle_array(game.questions);
        that.answer_pick_state = AnswerPickState.SHOW_ANSWERS;
    };

    this.onSocketMessage = function(socket, event, data) {
        if (event == 'answer_vote') {

        }
    };

    this.allowPause = function() { return true; };
    this.onPause = function(pause) {};

    this.sendDataMaster = function() {
        that.sh.__sendDataMaster(that.getDataMaster());
    };
    this.sendDataPlayer = function(player) {
        that.sh.__sendDataPlayer(player, that.getDataPlayer(player));
    };
    this.sendDataPlayers = function() {
        for (var p in that.game.players) {
            that.sendDataPlayer(p);
        }
    };
    this.sendDataAudienceMember = function(audience) {
        that.sh.__sendDataAudience(audience, that.getDataAudience(audience));
    };
    this.sendDataAudience = function() {
        for (var a in that.game.audience) {
            that.sendDataAudienceMember(a);
        }
    };

    this.getDataPlayer = function(player) {

    };

    this.getDataAudience = function(audience) {

    };

    this.getDataMaster = function() {
        var q = getCurrentQuestion();
        if (that.answer_pick_state == AnswerPickState.SHOW_ANSWERS) {
            return {
                question: q.question,
                round_state: that.answer_pick_state,
                answers: [
                    {
                        answer: q.answer1
                    },
                    {
                        answer: q.answer2
                    }
                ]
            };
        } else if (that.answer_pick_state == AnswerPickState.VOTE_ANSWERS) {
            return {
                question: q.question,
                round_state: that.answer_pick_state,
                answers: [
                    {
                        answer: q.answer1
                    },
                    {
                        answer: q.answer2
                    }
                ]
            };
        } else if (that.answer_pick_state == AnswerPickState.SHOW_SCORES) {
            var votes1 = [];
            var votes2 = [];
            var score1 = 0;
            var score2 = 0;
            for (var pid in q.answer1_vote_player_ids) {
                var player = that.game.getPlayerById(pid);
                if (player != null) {
                    score1 += SCORE_VOTE;
                    votes1.push({
                        name: player.name,
                        id: player.id
                    });
                }
            }
            for (var pid in q.answer2_vote_player_ids) {
                var player = that.game.getPlayerById(pid);
                if (player != null) {
                    score2 += SCORE_VOTE;
                    votes2.push({
                        name: player.name,
                        id: player.id
                    });
                }
            }
            if (votes1.isEmpty()) {
                score2 += SCORE_ALL_VOTES;
            } else if (votes2.isEmpty()) {
                score1 += SCORE_ALL_VOTES;
            } else if (votes1.length > votes2.length) {
                score1 += SCORE_WIN_VOTE;
            } else if (votes2.length > votes1.length) {
                score2 += SCORE_WIN_VOTE;
            }
            // TODO: add audience points and info
            return {
                question: q.question,
                round_state: that.answer_pick_state,
                answers: [
                    {
                        player: {
                            id: q.player1.id,
                            name: q.player1.name
                        },
                        answer: q.answer1,
                        votes: votes1,
                        audience_votes: 0, // TODO: impl
                        score: score1
                    },
                    {
                        player: {
                            id: q.player2.id,
                            name: q.player2.name
                        },
                        answer: q.answer2,
                        votes: votes2,
                        audience_votes: 0, // TODO: impl
                        score: score2
                    }
                ]
            };
        } else {
            return {
                question: q.question,
                round_state: that.answer_pick_state
            };
        }
    };

    var getCurrentQuestion = function() {
        return that.game.questions[that.question_index];
    };
    var nextQuestion = function() {
        that.question_index++;
        if (that.question_index == that.game.questions.length) {
            return false;
        }
        return true;
    };
};

module.exports = PickAndShowAnswersRound1;