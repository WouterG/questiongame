
var AnswerQuestionsRound1 = function(game, sh) {
    this.game = game;
    this.sh = sh;

    this.counter = 90;

    var that = this;
    var advance_on_resume = false;
    var advanced = false;
    var advance = function() {
        if (advanced) {
            return;
        }
        advanced = true;
        that.game.gamestate.advanceState();
    };

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() {
        var players = [];
        for (var i = 0; i < that.game.players.length; i++) {
            var p = that.game.players[i];
            players.push({
                name: p.name,
                id: p.id,
                done: p.is_done_answering
            });
        }
        return {
            counter: that.counter,
            players: players
        };
    };

    this.tick = function(game) {
        that.counter--;
        if (that.counter == 0) {
            advance();
        } else {
            that.sendDataMaster();
        }
    };
    this.start = function(game) {
        for (var player in game.players) {
            player.is_done_answering = false;
            player.current_questions.clear();
        }

        game.question_history.concat(game.questions);
        game.questions = [];
        var newQuestions = [];

        while (newQuestions.length < game.players.length) {
            var q = global.get_random_question();
            if (!game.historyHasQuestion(q) && !newQuestions.contains(q)) {
                newQuestions.push(q);
            }
        }

        var questionInstances = [];
        for (var i = 0; i < newQuestions.length; i++) {
            questionInstances.push(new Question(newQuestions[i]));
        }

        questionInstances = global.shuffle_array(questionInstances);

        for (var t = 1; t <= 2; t++) {
            for (var i = 0; i < questionInstances.length; i++) {
                var player = game.players[i];
                var question = questionInstances[i];
                question["player" + t] = player;
                player.current_questions.push(question);
            }
            var removed = questionInstances.shift();
            questionInstances.push(removed);
        }

        game.questions = questionInstances;

        that.sendDataPlayers();
        that.sendDataAudience();
        that.sendDataMaster();
    };

    this.onSocketMessage = function(socket, event, data) {
        if (socket.player == null || socket.game == null) {
            return;
        }
        if (socket.game != that.game) {
            return;
        }
        if (event == 'answer_change') {
            if (!global.has(data, "question_index", "question_answer")) {
                console.error('Invalid answer_change request');
                return;
            }
            if (socket.player.is_done_answering) {
                return;
            }
            if (socket.player.current_questions.length < 2) {
                return;
            }
            var question = socket.player.current_questions[data.question_index];
            for (var i = 1; i <= 2; i++) {
                if (question["player" + i] == socket.player) {
                    question["answer" + i] = data.question_answer;
                }
            }
        }
        if (event == 'answering_done') {
            if (socket.player.is_done_answering) {
                return;
            }
            if (socket.player.current_questions.length < 2) {
                return;
            }
            if (global.has(data, "question_answers")) {
                for (var ind = 0; ind < 2; ind++) {
                    var question = socket.player.current_questions[ind];
                    for (var i = 1; i <= 2; i++) {
                        if (question["player" + i] == socket.player) {
                            question["answer" + i] = data.question_answers[ind];
                        }
                    }
                }
            }
            socket.player.is_done_answering = true;
            var isEveryoneDone = true;
            for (var i = 0; i < that.game.players.length; i++) {
                if (!that.game.players[i].is_done_answering) {
                    isEveryoneDone = false;
                    break;
                }
            }
            if (isEveryoneDone) {
                that.counter = -1; // make sure we're past the timeout
                if (!that.game.paused) {
                    advance();
                } else {
                    advance_on_resume = true;
                }
            }
        }
    };

    this.allowPause = function() { return true; };
    this.onPause = function(pause) {
        if (!pause && advance_on_resume) {
            advance_on_resume = false;
            advance();
        }
    };

    this.sendDataMaster = function() {
        that.sh.__sendDataMaster(that.getStateData());
    };
    this.sendDataPlayer = function(player) {
        var questions = player.current_questions;
        that.sh.__sendDataPlayer(player, {
            questions: [
                questions[0].question,
                questions[1].question
            ]
        });
    };
    this.sendDataPlayers = function() {
        for (var player in that.game.players) {
            that.sendDataPlayer(player);
        }
    };
    this.sendDataAudienceMember = function(audience) {
        that.sh.__sendDataAudienceMember(audience);
    };
    this.sendDataAudience = function() {
        that.sh.__sendDataAudience();
    };
};

module.exports = AnswerQuestionsRound1;