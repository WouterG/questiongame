var PreGameCountdown = function(game, sh) {
    this.game = game;
    this.sh = sh;

    this.counter = 10;

    var that = this;

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() {
        return {
            counter: that.counter
        };
    };

    this.tick = function(game) {
        that.counter--;
        if (that.counter == 0) {
            that.game.gamestate.advanceState();
        } else if (that.counter > 0) {
            that.sendDataMaster();
        }
    };

    this.start = function(game) {
        that.sh.__sendDataAudience(that.getStateData());
        that.sh.__sendDataPlayers(that.getStateData());
    };

    this.allowPause = function() {
        return true;
    };

    this.onPause = function(pause) {
        if (!pause) {
            that.start();
        }
    };

    this.sendDataMaster = function() {
        that.sh.__sendDataMaster(that.getStateData());
    };

    this.sendDataPlayer = function(player) {};

    this.sendDataPlayers = function() {};

    this.sendDataAudienceMember = function(audience) {};

    this.sendDataAudience = function() {};
};

module.exports = PreGameCountdown;