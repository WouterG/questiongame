var Game = function(socket, keys) {

    this.master = socket;
    this.play_key = keys.play;
    this.audience_key = keys.watch;
    this.players = [];
    this.audience = {};
    this.running = false; // only changes after init to play, not used when paused
    this.paused = false;
    this.gamestate = new GameState(this);
    this.ticker = null;

    this.missing_players = {};

    this.question_history = [];
    this.questions = [];

    var that = this;

    this.addAudienceMember = function(member) {
        that.audience[member.id] = member;
    };

    this.removeAudienceMember = function(member) {
        delete that.audience[member.id];
    };

    this.historyHasQuestion = function(txt) {
        var f = false;
        for (var i = 0; i < that.question_history.length; i++) {
            if (that.question_history[i].question == txt) {
                f = true;
                break;
            }
        }
        return f;
    };

    this.addPlayer = function(player) {
        if (that.running) {
            return;
        }
        if (that.players.isEmpty()) {
            player.master = true;
        }
        player.id = that.players.length;
        that.players.push(player);
    };

    this.removePlayer = function(player) {
        if (that.running) {
            return;
        }
        var id = that.players.indexOf(player);
        if (id > -1) {
            that.players.splice(id, 1);
        }
    };

    this.isFull = function() {
        return that.players.length >= 8;
    };

    this.hasEnoughPlayers = function() {
        return that.players.length > 2;
    };

    this.getPlayer = function(name) {
        var p = null;
        for (var i = 0; i < that.players.length; i++) {
            if (that.players[i].name == name) {
                p = that.players[i];
                break;
            }
        }
        return p;
    };

    this.getPlayerById = function(id) {
        var p = null;
        for (var i = 0; i < that.players.length; i++) {
            if (that.players[i].id == id) {
                p = that.players[i];
                break;
            }
        }
        return p;
    };

    this.hasPlayerName = function(name) {
        var k = false;
        for (var i = 0; i < that.players.length; i++) {
            if (that.players[i].name.toLowerCase() == name.toLowerCase().trim()) {
                k = true;
                break;
            }
        }
        return k;
    };

    this.setPlayerMissing = function(player) {
        that.missing_players[player.name] = player;
    };

    this.setMissingPlayerReturned = function(player) {
        delete that.missing_players[player.name];
    };

    this.updatePlayerList = function() {
        var pl = [];
        for (var i = 0; i < that.players.length; i++) {
            if (i == 0) {
                that.players[i].master = true;
            } else {
                that.players[i].master = false;
            }
            pl.push(that.players[i].name);
        }
        that.sendMaster('playerlist_update', {
            players: pl,
            host: that.players[0].name
        });
    };

    this.sendMaster = function(key, obj) {
        that.master.emit(key, obj);
    };

    this.sendMasterPlayer = function(key, obj) {
        if (that.players.length > 0) {
            that.players[0].socket.emit(key, obj);
        }
    };

    this.sendPlayers = function(key, obj) {
        for (var i = 0; i < that.players.length; i++) {
            that.players[i].socket.emit(key, obj);
        }
    };

    this.sendAudience = function(key, obj) {
        for (var i = 0; i < that.audience.length; i++) {
            that.audience[i].socket.emit(key, obj);
        }
    };

    this.sendAll = function(key, obj) {
        that.sendMaster(key, obj);
        that.sendPlayers(key, obj);
        that.sendAudience(key, obj);
    };

    this.pauseGame = function(reason, toggleable) {
        var canPause = that.gamestate.current_state.allowPause();
        if (canPause) {
            that.paused = true;
            that.gamestate.current_state.onPause(true);
            that.sendAll('pause_data', {
                pause: true,
                message: reason,
                can_toggle: toggleable
            });
        }
        return canPause;
    };

    this.resumeGame = function() {
        var canResume = that.paused;
        if (canResume) {
            that.paused = false;
            that.gamestate.current_state.onPause(false);
            that.sendAll('pause_data', {
                pause: false,
                message: "( ?� ?? ?�)",
                can_toggle: true
            });
        }
    };

    this.onMessage = function(socket, event, data) {
        that.gamestate.sendState("onSocketMessage", socket, event, data);
    };

    this.start = function() {
        if (that.running) {
            return false;
        }
        that.running = true;
        that.ticker = setInterval(function() {
            if (!that.paused) {
                that.gamestate.current_state.tick(that);
            }
        }, 1000);
        if (that.gamestate.current_state.getProtocolState() == 'init') {
            that.gamestate.advanceState();
            return true;
        }
        return false;
    };

    this.restart = function() {
        if (that.ticker != null) {
            clearInterval(that.ticker);
        }
        that.running = false;
        that.questions.clear();
        if(that.question_history.length > (3/4) * global.config.questions.length) {
            that.question_history.clear();
        }
        // TODO: clean up (more) variables
        that.start();
    };

    /**
     * 0 params is normal close,
     * 1 param is close reason & error to true
     */
    this.close = function() {
        var packet = 'game_closed';
        var msg = {
            error: false,
            message: "The game has ended, thank you for playing"
        };
        if (arguments.length > 0) {
            msg.message = arguments[0];
            msg.error = true;
        }
        that.sendAudience(packet, msg);
        that.sendPlayers(packet, msg);
    };

    this.sendMaster('game_created', {
        play_key: that.play_key,
        audience_key: that.audience_key
    }); // game initialized packet

}

module.exports = Game;