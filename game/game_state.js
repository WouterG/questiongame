var GameState = function(game) {
    this.game = game;
    this.index = 0;

    var that = this;

    var applyState = function() {
        var stateClass = require('./states/' + GameState.gamestates[that.index]);
        that.current_state = new stateClass(that.game, SocketHelper);
        that.current_state.state_name = GameState.gamestates[that.index];
        that.current_state.start(that.game);
    };

    this.advanceState = function() {
        var flag = false;
        if (arguments.length > 0) {
            that.index = arguments[0];
            flag = true;
        }
        if (that.index >= GameState.gamestates.length || that.index < -1) {
            return false;
        }
        if (!flag) {
            that.index++;
        }
        applyState();
        return true;
    };

    /**
     * Gets state ahead or backwards from the current state, for instance getStateOffset(1) gets the next state.
     * @param offset Amount to offset
     * @returns {*} either a state class or null
     */
    this.getStateOffset = function(offset) {
        var k = GameState.gamestates[that.index + offset];
        if (k == undefined) {
            return null;
        }
        return k;
    };

    this.getCurrentStateName = function() {
        return GameState.gamestates[that.index];
    };

    // Abstract state, every state in ./states should follow.
    this.current_state = function(game, sh) {

        var f = function() {
            console.log('Invalid call to game.current_state, not yet initialized.');
            return 'no impl.';
        };

        /* Abstract */
        this.getProtocolState = f; // "init" / "play"

        this.tick = f;
        this.start = f;

        this.allowPause = f;
        this.onPause = f;

        this.sendDataMaster = f;
        this.sendDataPlayer = f;
        this.sendDataPlayers = f;
        this.sendDataAudienceMember = f;
        this.sendDataAudience = f;
    };

    this.sendState = function(func) {
        var f = that.current_state[func];
        if (f == undefined) {
            return;
        }
        if (typeof f != "function") {
            return;
        }
        var args = arguments;
        args.shift();
        if (args.length == 0) {
            f();
        } else {
            f.apply(undefined, args);
        }
    };

    var __sh = function() {
        this.__sendDataMaster = function(data) {
            that.game.sendMaster('state_update', {
                name: that.current_state.state_name,
                allow_pause: that.current_state.allowPause(),
                data: data
            });
        };
        this.__sendDataPlayer = function(player, data) {
            player.socket.emit('state_update', {
                name: that.current_state.state_name,
                allow_pause: that.current_state.allowPause(),
                data: data
            });
        };
        this.__sendDataPlayers = function(data) {
            that.game.sendPlayers('state_update', {
                name: that.current_state.state_name,
                allow_pause: that.current_state.allowPause(),
                data: data
            });
        };
        this.__sendDataAudienceMember = function(amember, data) {
            amember.socket.emit('state_update', {
                name: that.current_state.state_name,
                allow_pause: that.current_state.allowPause(),
                data: data
            });
        };
        this.__sendDataAudience = function(data) {
            that.game.sendAudience('state_update', {
                name: that.current_state.state_name,
                allow_pause: that.current_state.allowPause(),
                data: data
            });
        };
    };
    var SocketHelper = new __sh();



    applyState();

};

GameState.gamestates = [
    // Init
    'players_joining',
    // Pre-game
    'pre_game_countdown',
    'explanation_game_total',
    // Round 1 -> 2 questions per person
    'explanation_round1',
    'answer_questions_round1',
    'pick_and_show_answers_round1', // TODO
    'scoreboard_show1', // TODO
    // Round 2 -> 2 questions per person
    'explanation_round2',
    'answer_questions_round2',
    'pick_and_show_answers_round2', // TODO
    'scoreboard_show2', // TODO
    // Round 3 -> Single question
    'explanation_round3',
    'answer_question_round3',
    'pick_answers_round3', // TODO
    'show_results_round3', // TODO
    // Final score
    'final_score' // TODO
];

module.exports = GameState;

/*

var PlayersJoining = function(game, sh) {
    this.game = game;
    this.sh = sh;

    var that = this;

    this.getProtocolState = function() { return "play"; };
    this.getStateData = function() { return {}; };

    this.tick = function(game) {};
    this.start = function(game) {};

    this.onSocketMessage = function(socket, event, data) {};

    this.allowPause = function() { return false; };
    this.onPause = function(pause) {};

    this.sendDataMaster = function() {};
    this.sendDataPlayer = function(player) {};
    this.sendDataPlayers = function() {};
    this.sendDataAudienceMember = function(audience) {};
    this.sendDataAudience = function() {};
};

module.exports = PlayersJoining;

 */