var GameManager = function() {

    this.games = {};

    var that = this;

    var KEYTYPE = {
        NUMERIC: false,
        CHARS: true
    };

    this.newGame = function(socket) {
        var keys = {
            play: that.makekey(3, KEYTYPE.CHARS),
            watch: that.makekey(3, KEYTYPE.NUMERIC)
        }
        var game = new Game(socket, keys);
        that.games[game.key] = game;
        socket.game = game;
        socket.role = SocketType.HOST;
        return game;
    };

    this.findGameSpectateKey = function(key) {
        var g = null;
        for (var i = 0; i < that.games.length; i++) {
            if (that.games[i].audience_key == key) {
                g = that.games[i];
                break;
            }
        }
        return g;
    };

    this.removeGame = function(game) {
        delete that.games[game.key];
        game.close();
    };

    this.onMessage = function(socket) {
        socket.on('new_game', function(noarg) {
            if (socket.game != null) {
                that.removeGame(socket.game);
                socket.game = null;
            }
            that.newGame(socket);
        });
        socket.on('watch_game', function(data) {
            if (!has(data, "key")) {
                console.log('invalid watch_game request');
                return;
            }
            var game = that.findGameSpectateKey(data.key);
            if (game == null) {
                socket.emit('joining_error', {
                    type: "NOT_FOUND",
                    message: "Invalid game id"
                });
                return;
            }
            var audienceMember = new AudienceMember(socket, game);
            game.addAudienceMember(audienceMember);
            socket.emit('game_watching', {
                user_id: audienceMember.id
            });
            socket.audience_member = audienceMember;
            socket.role = SocketType.AUDIENCE;
            socket.game = game;
            if (game.running) {
                game.gamestate.current_state.sendDataAudienceMember(audienceMember);
            }
        });
        socket.on('join_game', function(data) {
            if (!has(data, "key", "name", "rejoin_password")) {
                console.info('invalid join_game request');
                return;
            }
            var user_role = 0;
            if (has(data, "admin")) {
                if (has(data.admin, "username", "password")) {
                    for (var i = 0; i < global.config.developers.length; i++) {
                        if (data.admin.username == global.config.developers[i].username) {
                            if (data.admin.password == global.config.developers[i].password) {
                                user_role = global.config.developers[i].level;
                            }
                        }
                    }
                }
            }
            var game = that.games[data.key];
            if (game == undefined) {
                socket.emit('joining_error', {
                    type: "NOT_FOUND",
                    message: "Invalid game id"
                });
                return;
            }
            if (data.name.length > 16) {
                socket.emit('joining_error', {
                    type: "NAME_LENGTH",
                    message: "Username is too long, max is 16 characters"
                });
                return;
            }
            if (data.name.length < 2) {
                socket.emit('joining_error', {
                    type: "NAME_LENGTH",
                    message: "Username is too short, min is 2 characters"
                });
                return;
            }
            if (game.hasPlayerName(data.name)) {
                var existingPlayer = game.getPlayer(data.name);
                if (!existingPlayer.connected) {
                    if (existingPlayer.authenticate(data.rejoin_password)) {
                        existingPlayer.socket = socket;
                        existingPlayer.socket.emit('game_joined', {
                            name: existingPlayer.name
                        });
                        socket.player = existingPlayer;
                        socket.role = SocketType.PLAYER;
                        socket.game = game;
                        game.setMissingPlayerReturned(existingPlayer);
                        socket.emit('game_joined', {
                            name: player.name
                        });
                        game.gamestate.current_state.sendDataPlayer(existingPlayer);
                        return;
                    } else {
                        socket.emit('joining_error', {
                            type: "INVALID_PASSWORD",
                            message: "The password was incorrect"
                        });
                        return;
                    }
                } else {
                    socket.emit('joining_error', {
                        type: "NAME_TAKEN",
                        message: "There is already a user in the game with that name"
                    });
                    return;
                }
            }
            if (game.isFull()) {
                socket.emit('joining_error', {
                    type: "GAME_FULL",
                    message: "This game already has 8 players"
                });
                return;
            }
            if (game.running) {
                socket.emit('joining_error', {
                    type: "GAME_RUNNING",
                    message: "This game is already running"
                });
                return;
            }
            var player = new Player(socket, data.name.trim(), data.rejoin_password);
            game.addPlayer(player);
            game.updatePlayerList();
            player.app_role = user_role;
            player.socket.player = player;
            player.socket.game = game;
            player.socket.role = SocketType.PLAYER;
            player.socket.emit('game_joined', {
                name: player.name
            });
        });
        socket.on('start_game', function(noarg) {
            if (socket.player == null) {
                socket.emit('error', {
                    message: "Your connection isn't marked as player"
                });
                return;
            }
            if (!socket.player.master) {
                socket.emit('error', {
                    message: "Your player isn't the master of the current game"
                });
                return;
            }
            if (socket.game == null) {
                socket.emit('error', {
                    message: "Your player isn't connected to a game?"
                });
                return;
            }
            if (!socket.game.hasEnoughPlayers()) {
                socket.emit('error', {
                    message: "There aren't enough players in this game, 3 needed"
                });
                return;
            }
            if (!socket.game.start()) {
                socket.emit('error', {
                    message: 'Game was already started or has ended'
                });
            }
        });
    };

    this.makekey = function(len, chars)
    {
        var tries = 0;
        var mkkey = function(l, c) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (!c) {
                possible = "0123456789";
            }
            for (var i = 0; i < l; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        };
        var text = null;
        while (text == null) {
            var p = Math.floor(tries / 5);
            tries++;
            text = mkkey(len + p, chars);
            var exists = false;
            for (var i = 0; i < that.games.length; i++) {
                if (chars) {
                    if (that.games[i].play_key == text) {
                        exists = true;
                        break;
                    }
                } else {
                    if (that.games[i].audience_key == text) {
                        exists = true;
                        break;
                    }
                }
            }
            if (exists) {
                text = null;
            }
        }
        return text;
    };

};

module.exports = GameManager;