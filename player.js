

var Player = function(socket, name, rejoin_password) {
    this.socket = socket;
    this.id = -1; // also index of player map
    this.name = name;
    this.rejoin_password = rejoin_password;
    this.connected = true;

    this.current_questions = [];
    this.is_done_answering = false;

    this.master = false;
    /**
     * Permission number, based on the master server, not the current game
     * @type {number}
     */
    this.app_role = 0; // for future -- maybe implement dev/admin features

    this.points = 0;

    var that = this;

    this.authenticate = function(password) {
        if (that.rejoin_password == password) {
            return true;
        }
        return false;
    };

};

module.exports = Player;