global.config = require('./config/config').getConfig();

require('./globals/global_imports');
require('./globals/global_utils');

global.wants_restart = false;

setInterval(function() {
    if (global.wants_restart) {
        console.warn('[WARNING] App requires restart, unsafe actions were performed!');
    }
}, 60 * 1000);

process.stdin.on('data', function(chunk) {
    if (chunk.toString().trim().toLowerCase() == 'stop') {
        process.exit();
    } else if (chunk.toString().trim().toLowerCase() == 'reload') {
        var oq = global.config.questions.length;
        global.config = reload_require('./config/config').getConfig();
        var nq = global.config.questions.length;
        var dq = nq - oq;
        if (dq > 0) {
            console.log('Reloaded config, ' + dq + ' questions added');
        } else if (dq == 0) {
            console.log('Reloaded config, no questions added');
        } else {
            dq = dq * -1;
            console.log('Reloaded config, ' + dq + ' questions removed');
            console.error('[ERROR] Removing questions during runtime triggers the restart messaging');
            global.wants_restart = true;
        }
    } else if (chunk.toString().trim().toLowerCase() == 'disablespam') {
        console.log('Disabled the restart required messages');
        global.wants_restart = false;
    } else {
        console.log('Command not recognized');
    }
});

global.io = require('socket.io').listen(global.config.port);
global.gamemanager = new GameManager();

global.io.on('connection', function(socket) {

    global.patch_socket(socket);

    socket.game = null;
    socket.role = SocketType.NONE;

    socket.audience_member = null;
    socket.player = null;

    socket.on('disconnect', function() {
        if (socket.game != null) {
            if (socket.role.isHost()) {
                global.gamemanager.removeGame(socket.game);
            } else if (socket.role.isAudience()) {
                socket.game.removeAudienceMember(socket.audience_member);
            } else if (socket.role.isPlayer()) {
                if (socket.game.gamestate.current_state.getProtocolState() == 'init') {
                    socket.game.removePlayer(socket.player);
                    socket.game.updatePlayerList();
                } else {
                    socket.player.connected = false;
                    socket.game.setPlayerMissing(socket.player);
                }
            }
        }
    });

    socket.on("*", function(data) {
        var eventName = data.__original_event;
        delete data["__original_event"];
        if (socket.game != null) {
            socket.game.onMessage(socket, eventName, data);
        }
    });

    global.gamemanager.onMessage(socket);
});

console.log('Listening on port ' + global.config.port);
