var AudienceMember = function(socket, game) {
    this.id = AudienceMember.getNextId();
    this.socket = socket;
    this.game = game;
};

AudienceMember.id_counter = 0;
AudienceMember.getNextId = function() {
    return AudienceMember.id_counter++;
};

module.exports = AudienceMember;