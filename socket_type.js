var SocketType = function(n) {
    this.n = n;
    var that = this;

    this.isHost = function() {
        return that.n == SocketType.HOST;
    };

    this.isPlayer = function() {
        return that.n == SocketType.PLAYER;
    };

    this.isAudience = function() {
        return that.n == SocketType.AUDIENCE;
    };
};

SocketType.NONE = new SocketType(0);
SocketType.AUDIENCE = new SocketType(1);
SocketType.PLAYER = new SocketType(2);
SocketType.HOST = new SocketType(3);

module.exports = SocketType;