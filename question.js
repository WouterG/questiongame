var Question = function(question) {

    this.question = question;
    this.player1 = null;
    this.player2 = null;

    this.answer1 = null;
    this.answer2 = null;

    this.answer1_vote_player_ids = [];
    this.answer2_vote_player_ids = [];

    this.answer1_vote_audience_ids = [];
    this.answer2_vote_audience_ids = [];
}
