/***
 * clear a dependency from the cache and then require() it again.
 * @param name dependency to re-require
 * @returns {*} the require() return value
 */
global.reload_require = function(name) {
    try {
        var name = require.resolve(name);
        if (name != undefined && name != null) {
            delete require.cache[name];
        }
    } catch (err) {
    }
    return require(name);
};

/***
 * Accepts 2 or more parameters. First parameter is an object,<br />
 * Every next parameter must be a string. Function will return true if the object<br />
 * hasOwnProperty(string_n) of every string argument.
 * @returns {boolean} if the object has all keys
 */
global.has = function() {
    if (arguments.length < 2) {
        return true;
    }
    var obj = arguments[0];
    var k = true;
    for (var i = 1; i < arguments.length ;i++) {
        if (!obj.hasOwnProperty(arguments[i])) {
            k = false;
            break;
        }
    }
    return k;
};

/**
 * Adds global "*" event that fires on all events
 * @param socket
 */
global.patch_socket = function(socket) {
    var onevent = socket.onevent;
    socket.onevent = function() {
        onevent.apply(this, arguments);
        if (arguments.length > 0 && arguments[0].type == 2) {
            var k = arguments[0].data[0];
            if (arguments[0].data[1] == undefined) {
                arguments[0].data[1] = {};
            }
            arguments[0].data[1]["__original_event"] = k;
            arguments[0].data[0] = "*";
            onevent.apply(this, arguments);
        }
    };
};

/**
 * Returns a random question from the global.config.questions list
 * @returns {*}
 */
global.get_random_question = function() {
    return global.config.questions[Math.floor(Math.random() * global.config.questions.length)];
};

/**
 * Generate a random number between min,max or just max, which takes 0 as min
 * @param min
 * @param max
 * @returns {*}
 */
global.random_number = function(min, max) {
    if (max == undefined) {
        max = min;
        min = 0;
    }
    return min + Math.floor(Math.random() * (max - min));
};

/**
 * Shuffles an array
 * @param o
 * @returns {*}
 */
global.shuffle_array = function(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}