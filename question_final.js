var FinalQuestion = function(question) {

    this.question = question;
    this.answers = {};

    var that = this;

    this.setAnswer = function(player, answer) {
        that.answers[player.id] = answer;
    };

    this.getAnswer = function(player) {
        return that.answers[player.id];
    };

};

module.exports = FinalQuestion;